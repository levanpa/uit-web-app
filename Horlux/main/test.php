
<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

	 <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

</head>

<body>
	<div class="slick-slider" id="single_item">
		<div> <img src="images/items/1.jpg"></div>
		<div> <img src="images/items/2.jpg"></div>
		<div> <img src="images/items/3.jpg"></div>
	</div> <!-- slider-nav.// -->
	

<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script type="text/javascript">
  $("#single_item").slick({
    dots: true
  });
</script>
</body>