<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Horlux - The best BIDDING place ever</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

	<!-- Bootstrap-->
	<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
	<link href="css/bootstrap-custom.css" rel="stylesheet" type="text/css"/>

	<!-- Font awesome -->
	<link href="fonts/fontawesome/css/all.css" type="text/css" rel="stylesheet">

	<!-- custom style -->
	<link href="css/uikit.css" rel="stylesheet" type="text/css"/>
	<link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />
	<link rel="stylesheet" type="text/css" href="css/custom.css">

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body> 
	<div class="space"></div>
	<header class="section-header fixed-top space" id="header-on-top">
		<section class="header-main">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-md-9 col-sm-8" name="header-left">
						<div class="brand-wrap">
							<a href="home.php" target="_self">
								<img class="logo" src="images/logos/logo.jpg">
							</a>
						</div> <!-- brand-wrap.// -->					
						<form action="#" id="search-form">
							<div class="input-group w-100">
								<!-- <select class="custom-select"  name="category_name">
									<option value="">All type</option><option value="codex">Special</option>
									<option value="comments">Only best</option>
									<option value="content">Latest</option>
								</select> -->
								<input type="text" class="form-control" style="width:60%;" placeholder="Search product">

								<div class="input-group-append">
									<button class="btn btn-primary" type="submit">
										<i class="fa fa-search"></i>
									</button>
								</div>
							</div>
						</form> <!-- search-wrap .end// -->
					</div>
					<div class="col-lg-4 col-md-3 col-sm-4" name="header-right">
						<div class="d-flex justify-content-center">
							<div class="widget-header">	
								<small>Hello guest!</small>
								<div class="text-wrap dropdown">
									<a href="#" class="text-primary" data-toggle="dropdown" aria-expanded="false" data-offset="20,10">
										Sign in <i class="fa fa-caret-down"></i> 
									</a> 										
									<div class="dropdown-menu dropdown-menu-right">
										<?php include("signin.php"); ?>
									</div>
								</div>	
							</div>
							<a href="cart.php" class="widget-header border-left pl-2 ml-2" target="_self">
								<div class="icontext">
									<div class="icon-wrap icon-sm round border"><i class="fa fa-shopping-cart"></i>
									</div>
								</div>
								<span class="badge badge-pill badge-danger notify">0</span>
							</a>
							<a href="account.php" class="widget-header border-left pl-2 ml-2" target="_self">
								<div class="icontext">
									<div class="icon-wrap icon-sm round border"><i class="fa fa-user"></i>
									</div>
								</div>
							</a>
						</div>
					</div>  <!-- cart, acc.// -->
				</div> <!-- row.// -->
			</div> <!-- container.// -->
		</section> <!-- header-main .// -->
	</header>

	<script type="text/javascript">
		$(document).ready(function(){
			//adaptive height
			var w = $(document).width();
			if (w <= 576) {
				$(".space").css("height","130px");
			}
		});
	</script>
</body>
</html>