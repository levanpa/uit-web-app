<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Horlux - The best BIDDING place ever</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

	<!-- Bootstrap-->
	<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
	<link href="css/bootstrap-custom.css" rel="stylesheet" type="text/css"/>

	<!-- Font awesome -->
	<link href="fonts/fontawesome/css/all.css" type="text/css" rel="stylesheet">
	
	<!-- custom style -->
	<link href="css/uikit.css" rel="stylesheet" type="text/css"/>
	<link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />
	<link rel="stylesheet" type="text/css" href="css/custom.css">

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
	<?php include('header.php')?>

	<!-- ========================= navigation =================== -->
	<section class="bg2">
		<div class="container">
			<div class="row no-gutters">
				<div class="col-lg-9 offset-lg-5-24">
					<nav class="navbar navbar-expand-lg navbar-light">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
							<span class="navbar-toggler-icon"></span>
						</button>
						<div class="collapse navbar-collapse" id="main_nav">
							<ul class="navbar-nav">
								<li class="nav-item">
									<a class="nav-link" href="#"> Home </a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#">New arrival</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#">Populars</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#">Deals</a>
								</li>
								<li class="nav-item">
									<a class="nav-link" href="#">Last viewed</a>
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown07" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">More</a>
									<div class="dropdown-menu" aria-labelledby="dropdown07">
										<a class="dropdown-item" href="#">Foods and Drink</a>
										<a class="dropdown-item" href="#">Home interior</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="#">Category 1</a>
										<a class="dropdown-item" href="#">Category 2</a>
										<a class="dropdown-item" href="#">Category 3</a>
									</div>
								</li>
							</ul>
						</div> <!-- collapse .// -->
					</nav>
				</div> <!-- col.// -->
			</div> <!-- row.// -->
		</div> <!-- container .// -->
	</section>

	<!-- ========================= category, banner ========================= -->
	<section class="section-main bg padding-bottom">
		<div class="container">
			<div class="row no-gutters border border-top-0 bg-white">
				<aside class="col-lg-5-24 col-sm-4">
					<nav>
						<div class="title-category bg-secondary white d-none d-lg-block" style="margin-top:-53px">
							<span >Categories</span>
						</div>
						<ul class="menu-category d-none d-sm-block">
							<li> <a href="#">Food &amp Beverage </a></li>
							<li> <a href="#">Home Equipments </a></li>
							<li> <a href="#">Machinery Items </a></li>
							<li> <a href="#">Toys & Hobbies  </a></li>
							<li> <a href="#">Beauty & Personal Care  </a></li>
							<li> <a href="#">Mobile phones  </a></li>
							<li class="has-submenu"> <a href="#">More category  <i class="icon-arrow-right pull-right"></i></a>
								<ul class="submenu">
									<li> <a href="#">Food &amp Beverage </a></li>
									<li> <a href="#">Home Equipments </a></li>
									<li> <a href="#">Machinery Items </a></li>
									<li> <a href="#">Toys & Hobbies  </a></li>
									<li> <a href="#">Consumer Electronics  </a></li>
									<li> <a href="#">Home & Garden  </a></li>
									<li> <a href="#">Beauty & Personal Care  </a></li>
								</ul>
							</li>
						</ul>
					</nav>
				</aside> <!-- col.// -->
				<main class="col-lg-19-24 col-sm-8">
					<div class="card-banner overlay-grad" style="background-image: url('images/posts/1.jpg');">
						<div class="card-body white">
							<h2 class="card-title">Horlux.xyz</h2>
							<h5 class="card-text">The best bidding place you have ever met.</h5>
						</div>
					</div>
				</main> <!-- col.// -->
			</div> <!-- row.// -->
		</div> <!-- container .//  -->
	</section>

	<!-- ====================== products grid ======================= -->
	<section class="section-content padding-y">
		<div class="container">
			<header class="section-heading">
				<h3 class="title-section">Some products are running out of time!!</h3>
			</header>
			<div class="row" name="product-grid">
				<div class="col-md-3 col-sm-6">
					<figure class="card card-product">
						<div class="img-wrap"> 
							<img id="product-img" src="images/items/3.jpg" onmouseover="mouseOver();" onmouseout="mouseOut();">
							<a class="btn-overlay" href="#"><i class="fa fa-search-plus"></i> Quick view</a>
						</div>
						<figcaption class="info-wrap">
							<a href="product-detail.php" class="title">Good item name</a>
							<p class="countdown-time">Ending in <span class="count5">05:00</span></p>
							<div class="action-wrap">
								<a href="product-detail.php" class="btn btn-primary btn-sm float-right"> Detail </a>
								<div class="price-wrap h5">
									<span class="price-new">1 VND</span>
									<!-- <del class="price-old">$1980</del> -->
								</div> <!-- price-wrap.// -->
							</div> <!-- action-wrap -->
						</figcaption>
					</figure> <!-- card // -->
				</div> <!-- col // -->
				<div class="col-md-3 col-sm-6">
					<figure class="card card-product">
						<div class="img-wrap"> <img src="images/items/4.jpg">
							<a class="btn-overlay" href="#"><i class="fa fa-search-plus"></i> Quick view</a>
						</div>
						<figcaption class="info-wrap">
							<a href="#" class="title">The name of product</a>
							<p class="countdown-time">Ending in <span class="count3">03:00</span></p>
							<div class="action-wrap">
								<a href="#" class="btn btn-primary btn-sm float-right"> Detail </a>
								<div class="price-wrap h5">
									<span class="price-new">1 VND</span>
								</div> <!-- price-wrap.// -->
							</div> <!-- action-wrap -->
						</figcaption>
					</figure> <!-- card // -->
				</div> <!-- col // -->
				<div class="col-md-3 col-sm-6">
					<figure class="card card-product">
						<div class="img-wrap"> <img src="images/items/5.jpg">
							<a class="btn-overlay" href="#"><i class="fa fa-search-plus"></i> Quick view</a>
						</div>
						<figcaption class="info-wrap">
							<a href="#" class="title">Name of product</a>
							<p class="countdown-time">Ending in <span class="count1">01:00</span></p>
							<div class="action-wrap">
								<a href="#" class="btn btn-primary btn-sm float-right"> Detail </a>
								<div class="price-wrap h5">
									<span class="price-new">1 VND</span>
								</div> <!-- price-wrap.// -->
							</div> <!-- action-wrap -->
						</figcaption>
					</figure> <!-- card // -->
				</div> <!-- col // -->
				<div class="col-md-3 col-sm-6">
					<figure class="card card-product">
						<div class="img-wrap"> <img src="images/items/6.jpg">
							<a class="btn-overlay" href="#"><i class="fa fa-search-plus"></i> Quick view</a>
						</div>
						<figcaption class="info-wrap">
							<a href="#" class="title">The name of product</a>
							<p class="countdown-time">Ending in <span class="count3">03:00</span></p>
							<div class="action-wrap">
								<a href="#" class="btn btn-primary btn-sm float-right"> Detail </a>
								<div class="price-wrap h5">
									<span class="price-new">1 VND</span>
								</div> <!-- price-wrap.// -->
							</div> <!-- action-wrap -->
						</figcaption>
					</figure> <!-- card // -->
				</div> <!-- col // -->
				<div class="col-md-3 col-sm-6">
					<figure class="card card-product">
						<div class="img-wrap"> 
							<img src="images/items/3.jpg">
							<a class="btn-overlay" href="#"><i class="fa fa-search-plus"></i> Quick view</a>
						</div>
						<figcaption class="info-wrap">
							<a href="#" class="title">Good item name</a>
							<p class="countdown-time">Ending in <span class="count2">2:00</span></p>
							<div class="action-wrap">
								<a href="#" class="btn btn-primary btn-sm float-right"> Detail </a>
								<div class="price-wrap h5">
									<span class="price-new">1 VND</span>
									<!-- <del class="price-old">$1980</del> -->
								</div> <!-- price-wrap.// -->
							</div> <!-- action-wrap -->
						</figcaption>
					</figure> <!-- card // -->
				</div> <!-- col // -->
				<div class="col-md-3 col-sm-6">
					<figure class="card card-product">
						<div class="img-wrap"> <img src="images/items/4.jpg">
							<a class="btn-overlay" href="#"><i class="fa fa-search-plus"></i> Quick view</a>
						</div>
						<figcaption class="info-wrap">
							<a href="#" class="title">The name of product</a>
							<p class="countdown-time">Ending in <span class="count5">05:00</span></p>
							<div class="action-wrap">
								<a href="#" class="btn btn-primary btn-sm float-right"> Detail </a>
								<div class="price-wrap h5">
									<span class="price-new">1 VND</span>
								</div> <!-- price-wrap.// -->
							</div> <!-- action-wrap -->
						</figcaption>
					</figure> <!-- card // -->
				</div> <!-- col // -->
				<div class="col-md-3 col-sm-6">
					<figure class="card card-product">
						<div class="img-wrap"> <img src="images/items/5.jpg">
							<a class="btn-overlay" href="#"><i class="fa fa-search-plus"></i> Quick view</a>
						</div>
						<figcaption class="info-wrap">
							<a href="#" class="title">Name of product</a>
							<p class="countdown-time">Ending in <span class="count4">04:00</span></p>
							<div class="action-wrap">
								<a href="#" class="btn btn-primary btn-sm float-right"> Detail </a>
								<div class="price-wrap h5">
									<span class="price-new">1 VND</span>
								</div> <!-- price-wrap.// -->
							</div> <!-- action-wrap -->
						</figcaption>
					</figure> <!-- card // -->
				</div> <!-- col // -->
				<div class="col-md-3 col-sm-6">
					<figure class="card card-product">
						<div class="img-wrap"> <img src="images/items/6.jpg">
							<a class="btn-overlay" href="#"><i class="fa fa-search-plus"></i> Quick view</a>
						</div>
						<figcaption class="info-wrap">
							<a href="#" class="title">The name of product</a>
							<p class="countdown-time">Ending in <span class="count2">02:00</span></p>
							<div class="action-wrap">
								<a href="#" class="btn btn-primary btn-sm float-right"> Detail </a>
								<div class="price-wrap h5">
									<span class="price-new">1 VND</span>
								</div> <!-- price-wrap.// -->
							</div> <!-- action-wrap -->
						</figcaption>
					</figure> <!-- card // -->
				</div> <!-- col // -->
			</div> <!-- row.// -->
		</div> <!-- container .//  -->
	</section>

	<table class="table table-striped">
		
	</table>

	<?php include('footer.php');?>
	
	<!-- <script type="text/javascript" src="js/custom.js"></script> -->
	<script type="text/javascript">
		
		$(document).ready(function(){
			//product img animation
			$("#product-img").mouseenter(function(){
				$(this).attr("src","images/items/7.jpg").hide().fadeIn(500);		
			});
			$("#product-img").mouseleave(function(){
				$(this).attr("src","images/items/3.jpg").hide().fadeIn(500);
			});

		});	
	</script>
</body>
</html>