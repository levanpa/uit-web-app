
/*	
.col- (extra small devices - screen width < 576px)
.col-sm- (small devices - screen width >= 576px)
.col-md- (medium devices - screen width >= 768px)
.col-lg- (large devices - screen width >= 992px)
.col-xl- (xlarge devices - screen width >= 1200px)
*/

"use strict";
//countdown time 1-5 minutes
function startTimer(duration, display) {
	var timer = duration, minutes, seconds;
	setInterval(function () {
		minutes = parseInt(timer / 60, 10)
		seconds = parseInt(timer % 60, 10);
		minutes = minutes < 10 ? "0" + minutes : minutes;
		seconds = seconds < 10 ? "0" + seconds : seconds;

		display.textContent = minutes + ":" + seconds;
		if (--timer < 0) {
			display.textContent = "00:00"
		}
	}, 1000);
}//start timer

//load countdown timer
function loadCountdowntime(){
	var i = 0, length = 5;
	var displays;
	for(i; i<=length; i++){
		var displays = document.querySelectorAll('.count'+i);
		for (var j=0; j < displays.length; j++) {
			startTimer(i*60-3, displays[j]);
		}
	}
}

//load countdown timer at the begining
window.onload = loadCountdowntime;

//validate signup form
function checkPass() {
	var pass = document.forms["signup-form"]["signup-pass"].value;
	var repass = document.forms["signup-form"]["signup-repass"].value;
	var a = document.getElementById("signup-pass-area");
	if (pass.length < 6 || pass != repass) {        
		a.style.border = "1px solid red";
		a.style.borderRadius = "4px";
	} else {
		a.style.border = "";
	}
}

function checkPhone(){
	var phone = document.forms["signup-form"]["signup-phone"].value;
	var a = document.getElementById("signup-phone");
	if (phone.length>10 || phone.length<9) {
		a.style.border = "1px solid red";
		a.style.borderRadius = "4px";        
	} else {
		a.style.border = "";        
	}
}

function validateForm(){
	var a = document.getElementById("signup-pass-area");
	var b = document.getElementById("signup-phone");
	if(!(a.style.border == "" && b.style.border == "")){
		alert("Please input valid data");
		return false;
	}
}