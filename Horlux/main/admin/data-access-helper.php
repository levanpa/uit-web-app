<?php
class DataAccessHelper {
	private $conn;

	public function connect(){
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "demo1";

		// Create connection
		$GLOBALS['conn'] = new mysqli($servername, $username, $password, $dbname);

		// Check connection
		if ($GLOBALS['conn']->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
		mysql_query("set names 'utf8'"); //error this line
		echo "Connected successfully\r\n";
	}

	//update, delete, insert
	public function executeNonQuery($sql){
		if ($GLOBALS['conn']->query($sql) === TRUE) {
			echo "Your query has been executed successfully\r\n";
		} else {
			echo "Error: " . $sql . "<br>" . $GLOBALS['conn']->error;
		}
	}

	//select
	public function executeQuery($sql){
		$result = $GLOBALS['conn']->query($sql);
		return $result;
	}

	public function close(){
		mysqli_close($GLOBALS['conn']);
	}
}

?> 