<div class="card card-log">
  <article class="card-body">
    <a href="signup.php" class="float-right btn btn-outline-primary">Sign up</a>
    <h4 class="card-title mb-4 mt-1">Sign in</h4>
    <form>
      <div class="form-group">
       <label>Your email</label>
       <input name="" class="form-control" placeholder="Email" type="email" required>
     </div> <!-- form-group// -->
     <div class="form-group">
       <a class="float-right" href="#">Forgot?</a>
       <label>Your password</label>
       <input class="form-control" placeholder="Password" type="password" required>
     </div> <!-- form-group// --> 
     <div class="form-group"> 
      <div class="checkbox">
        <label> <input type="checkbox" id="save-pass-checkbox"> Save password </label>
      </div> <!-- checkbox .// -->
    </div> <!-- form-group// -->  
    <div class="form-group">
      <button type="submit" class="btn btn-primary btn-block"> Login  </button>
    </div> <!-- form-group// -->                                                       
  </form>
</article>
</div> <!-- card.// -->