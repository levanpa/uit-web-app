<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Horlux - The best BIDDING place ever</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

	<!-- Bootstrap-->
	<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
	<link href="css/bootstrap-custom.css" rel="stylesheet" type="text/css"/>

	<!-- Font awesome -->
	<link href="fonts/fontawesome/css/all.css" type="text/css" rel="stylesheet">

	<!-- custom style -->
	<link href="css/uikit.css" rel="stylesheet" type="text/css"/>
	<link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />
	<link rel="stylesheet" type="text/css" href="css/custom.css">

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
	<?php include('header.php');?>

	<div class="row">
		<!-- side-menu  -->
		<div class="col-sm-3 col-xs-12 ">
			<h4 class="title-section"> <strong>Le Van Pa</strong></h4>
			<ul class="list-bullet">			
				<li>Account management</li>
				<div class="side-menu-item">
					<i class="fas fa-user"></i> 
					<a id="edit-info" href="#">Edit informations</a>
				</div>
				<div class="side-menu-item">
					<i class="fas fa-map-marked"></i> 
					<a id="addresses" href="#">Addresses</a>
				</div>
				<div class="side-menu-item">
					<i class="far fa-credit-card"></i> 
					<a id="credit-card-info" href="#">Credit card information</a>
				</div>	
				<div class="side-menu-item">
					<i class="fa fa-lock"></i> 
					<a id="change-pass" href="#">Change password</a>
				</div>	

				<li>Orders management</li>
				<div class="side-menu-item">
					<i class="fas fa-file-invoice"></i>
					<a id="successful-orders" href="#">Successful orders</a>
				</div>				
				<div class="side-menu-item">
					<i class="fas fa-file-invoice"></i>
					<a id="cancelled-orders" href="#">Cancelled orders</a>
				</div>
			</ul>
		</div>

		<!-- main content  -->
		<div class="col-sm-9 col-xs-12 white-bg">
			<!-- edit-info -->
			<div class="container edit-info side-menu" style="display: block;">
				<h4 class="card-title mt-3 text-center">Edit User Information</h4>		
				<form name="edit-info-form" id="edit-info-form" action="" onsubmit="return validateForm();">
					<!-- name -->
					<div class="form-group input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-user"></i></span>
						</div>
						<input name="signup-name" class="form-control" placeholder="Full name" type="text" required>
					</div> 
					<!-- email -->
					<div class="form-group input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
						</div>
						<input name="signup-email" class="form-control" placeholder="Email address" type="email">
					</div>
					<!-- phone -->
					<div class="form-group input-group" id="signup-phone">
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-phone"></i> </span>
						</div>
						<input name="signup-phone" class="form-control" placeholder="Phone number" type="number" onchange="checkPhone();" required>
					</div>
					<!-- address -->
					<div class="form-group input-group">            
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-building"></i> </span>
						</div>
						<input type="text" class="form-control" name="signup-address" placeholder="Address">            
					</div>
					<!-- job type -->
					<div class="form-group input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-building"></i> </span>
						</div>
						<select class="form-control" name="signup-job">
							<option selected=""> Select job type</option>
							<option>Designer</option>
							<option>Manager</option>
							<option>Accaunting</option>
						</select>
					</div>							                                      
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block"> Update Information  </button>
					</div> <!-- form-group// -->
					<p id="error" class="text-error"></p>                                         
				</form>
			</div>
			<!-- addresses -->
			<div class="container addresses side-menu" style="display: none;">
				<h4 class="card-title mt-3 text-center">Addresses Information</h4>		
				<form name="addresses-form" id="addresses-form" action="">
					<!-- address -->
					<div class="form-group input-group">            
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-building"></i> </span>
						</div>
						<input type="text" class="form-control" placeholder="Your address">            
					</div>
					<!-- shipping address -->
					<div class="form-group input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-building"></i></span>
						</div>
						<input class="form-control" placeholder="Shipping address" type="text" required>
					</div> 
					<!-- billing address -->
					<div class="form-group input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-building"></i> </span>
						</div>
						<input class="form-control" placeholder="Billing address" type="text">
					</div>						

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block"> Update Information  </button>
					</div> <!-- form-group// -->
					<p id="error" class="text-error"></p>                                        
				</form>
			</div>
			<!-- change-pass -->
			<div class="container change-pass side-menu" style="display: none;">
				<h4 class="card-title mt-3 text-center">Change Password</h4>		
				<form name="change-pass-form" id="change-pass-form" action="">					
					<!-- Current password -->
					<div class="form-group input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-lock"></i></span>
						</div>
						<input class="form-control" placeholder="Current password" type="password" required>
					</div> 
					<!-- New password -->
					<div class="form-group input-group">
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-lock"></i> </span>
						</div>
						<input class="form-control" placeholder="New password" type="password" required>
					</div>
					<!-- repass -->
					<div class="form-group input-group">            
						<div class="input-group-prepend">
							<span class="input-group-text"> <i class="fa fa-lock"></i> </span>
						</div>
						<input type="password" class="form-control" placeholder="re-enter new password" required>    
					</div>					

					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">Update Information</button>
					</div> <!-- form-group// -->
					<p id="error" class="text-error"></p>                                       
				</form>
			</div>
			<!-- credit-card-info -->
			<div class="container credit-card-info side-menu" style="display: none;">
				<h4 class="card-title mt-3 text-center">Credit Card Information</h4>	
				<form name="credit-card-form">
					<div class="form-group">
						<input type="text" class="form-control" name="username" placeholder="Full name (on the card)" required="">
					</div> <!-- form-group.// -->

					<div class="form-group">
						<div class="input-group">
							<input type="text" class="form-control" name="cardNumber" placeholder="Card number" type="number">
							<div class="input-group-append">
								<span class="input-group-text text-muted">
									<i class="fab fa-cc-visa"></i> &nbsp; <i class="fab fa-cc-amex"></i> &nbsp; 
									<i class="fab fa-cc-mastercard"></i> 
								</span>
							</div>
						</div>
					</div> <!-- form-group.// -->

					<div class="row">
						<div class="col-sm-8">
							<div class="form-group">
								<label><span class="hidden-xs">Expiration</span> </label>
								<div class="input-group">
									<input type="number" class="form-control" placeholder="MM" name="">
									<input type="number" class="form-control" placeholder="YY" name="">
								</div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label data-toggle="tooltip" title="" data-original-title="3 digits code on back side of the card">CVV <i class="fa fa-question-circle"></i></label>
								<input type="number" class="form-control" required="">
							</div> <!-- form-group.// -->
						</div>
					</div> <!-- row.// -->
					<button class="subscribe btn btn-primary btn-block" type="button"> Update Information  </button>
				</form>
			</div>
			<!-- successful-orders -->
			<div class="container successful-orders side-menu" style="display: none;">
				<div class="box orders-list-box">
					<div class="card order-card">
						<figure class="itemside">
							<div class="aside">	<img class="img-sm" width="80" width="80" src="images/items/7.jpg"> </div>
							<figcaption class="text-wrap">
								<p class="title">Order <span class="bold">AF55212421</span><span> (3 items)</span></p> 
								<p>Life circle: 27/09/2018 - 02/10/2018</p>
								<p>Total price: <span class="text-primary">2.354.000vnd</span></p>
							</figcaption>
						</figure>  <!-- itemside.// -->
						<div class="d-flex justify-content-end btn-order-detail">
							<button class="btn btn-primary">View Detail</button>
						</div>
					</div>
					<div class="card order-card">
						<figure class="itemside">
							<div class="aside">	<img class="img-sm" width="80" width="80" src="images/items/7.jpg"> </div>
							<figcaption class="text-wrap">
								<p class="title">Order <span class="bold">AF55287341</span><span> (1 items)</span></p> 
								<p>Life circle: 01/10/2018 - 07/10/2018</p>
								<p>Total price: <span class="text-primary">1.004.000vnd</span></p>
							</figcaption>
						</figure>  <!-- itemside.// -->
						<div class="d-flex justify-content-end btn-order-detail">
							<button class="btn btn-primary">View Detail</button>
						</div>
					</div>
					<div class="card order-card">
						<figure class="itemside">
							<div class="aside">	<img class="img-sm" width="80" width="80" src="images/items/7.jpg"> </div>
							<figcaption class="text-wrap">
								<p class="title">Order <span class="bold">AF52562421</span><span> (1 items)</span></p> 
								<p>Life circle: 15/10/2018 - 20/10/2018</p>
								<p>Total price: <span class="text-primary">346.000vnd</span></p>
							</figcaption>
						</figure>  <!-- itemside.// -->
						<div class="d-flex justify-content-end btn-order-detail">
							<button class="btn btn-primary">View Detail</button>
						</div>
					</div>
				</div> <!-- box.// -->
			</div>
			<!-- cancelled-orders -->
			<div class="container cancelled-orders side-menu" style="display: none;">
				<div class="box orders-list-box">
					<div class="card order-card">
						<figure class="itemside">
							<div class="aside">	<img class="img-sm" width="80" width="80" src="images/items/7.jpg"> </div>
							<figcaption class="text-wrap">
								<p class="title">Order <span class="bold">AF52562421</span><span> (1 items)</span></p> 
								<p>Life circle: 15/10/2018 - 20/10/2018</p>
								<p>Total price: <span class="text-primary">346.000vnd</span></p>
							</figcaption>
						</figure>  <!-- itemside.// -->
						<div class="d-flex justify-content-end btn-order-detail">
							<button class="btn btn-primary">View Detail</button>
						</div>
					</div>
					<div class="card order-card">
						<figure class="itemside">
							<div class="aside">	<img class="img-sm" width="80" width="80" src="images/items/7.jpg"> </div>
							<figcaption class="text-wrap">
								<p class="title">Order <span class="bold">AF55212421</span><span> (3 items)</span></p> 
								<p>Life circle: 27/09/2018 - 02/10/2018</p>
								<p>Total price: <span class="text-primary">2.354.000vnd</span></p>
							</figcaption>
						</figure>  <!-- itemside.// -->
						<div class="d-flex justify-content-end btn-order-detail">
							<button class="btn btn-primary">View Detail</button>
						</div>
					</div>
					<div class="card order-card">
						<figure class="itemside">
							<div class="aside">	<img class="img-sm" width="80" width="80" src="images/items/7.jpg"> </div>
							<figcaption class="text-wrap">
								<p class="title">Order <span class="bold">AF55287341</span><span> (1 items)</span></p> 
								<p>Life circle: 01/10/2018 - 07/10/2018</p>
								<p>Total price: <span class="text-primary">1.004.000vnd</span></p>
							</figcaption>
						</figure>  <!-- itemside.// -->
						<div class="d-flex justify-content-end btn-order-detail">
							<button class="btn btn-primary">View Detail</button>
						</div>
					</div>					
				</div> <!-- box.// -->
			</div>			
		</div>
	</div>

	<?php include('footer.php');?>
	
	<script>
		$(document).ready(function(){
			//adaptive height			
			var w = $(document).width();
			if (w <= 576) {
				$(".btn-order-detail").removeClass("btn-order-detail");
			}

			//side menu click
			$("#edit-info").click(function(){
				$(".side-menu").css("display", "none");
				$(".edit-info").css("display", "block");
			});
			$("#addresses").click(function(){
				$(".side-menu").css("display", "none");
				$(".addresses").css("display", "block");
			});
			$("#credit-card-info").click(function(){
				$(".side-menu").css("display", "none");
				$(".credit-card-info").css("display", "block");
			});
			$("#successful-orders").click(function(){
				$(".side-menu").css("display", "none");
				$(".successful-orders").css("display", "block");
			});	
			$("#cancelled-orders").click(function(){
				$(".side-menu").css("display", "none");
				$(".cancelled-orders").css("display", "block");
			});
			$("#change-pass").click(function(){
				$(".side-menu").css("display", "none");
				$(".change-pass").css("display", "block");
			});

			//order-card click
			$(".btn-order-detail").click(function(){
				window.open("order-detail.php","_self");
			});
		});
	</script>
</body>
</html>

