<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Horlux - The best BIDDING place ever</title>
  <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

  <!-- Bootstrap-->
  <script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
  <link href="css/bootstrap-custom.css" rel="stylesheet" type="text/css"/>

  <!-- Font awesome -->
  <link href="fonts/fontawesome/css/all.css" type="text/css" rel="stylesheet">
  
  <!-- custom style -->
  <link href="css/uikit.css" rel="stylesheet" type="text/css"/>
  <link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />
  <link rel="stylesheet" type="text/css" href="css/custom.css">

  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
  <?php include('header.php');?>
  

  <?php include('footer.php');?>

</body>
</html>