<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Horlux - The best BIDDING place ever</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

	<!-- Bootstrap-->
	<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
	<link href="css/bootstrap-custom.css" rel="stylesheet" type="text/css"/>

	<!-- Font awesome -->
	<link href="fonts/fontawesome/css/all.css" type="text/css" rel="stylesheet">
	
	<!-- custom style -->
	<link href="css/uikit.css" rel="stylesheet" type="text/css"/>
	<link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />
	<link rel="stylesheet" type="text/css" href="css/custom.css">

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
	<!-- section-header.// -->
	<?php include('header.php');?>
	
	<!-- ========================= SECTION CONTENT ========================= -->
	<div class="row">
		<div class="col-sm-6 col-xs-12">
			<div class="list-group">
				<article class="list-group-item">
					<header class="filter-header">
						<a href="#" data-toggle="collapse" data-target="#collapse1">
							<i class="icon-action fa fa-chevron-down"></i>
							<h6 class="title">Addresses</h6>
						</a>
					</header>
					<div class="filter-content collapse show" id="collapse1">		
						<div class="box">
							<dl>
								<dt>Shipping address: </dt>
								<dd> Dolor sit amet, consectetur adipisicing elit do eiusmod
								tempor incididunt</dd>
							</dl>
							<dl>
								<dt>Billing address: </dt>
								<dd>Dolor sit amet, consectetur adipisicing elit do eiusmod
								tempor incididunt</dd>
							</dl>
						</div> <!-- box.// -->
					</div> <!-- collapse -filter-content  .// -->
				</article>
			</div>
		</div>
		<div class="col-sm-6 col-xs-12">
			<div class="list-group">
				<article class="list-group-item">
					<header class="filter-header">
						<a href="#" data-toggle="collapse" data-target="#collapse2">
							<i class="icon-action fa fa-chevron-down"></i>
							<h6 class="title">Payment method</h6>
						</a>
					</header>
					<div class="filter-content collapse show" id="collapse2">
						<?php include("payment-method.php"); ?>
					</div>
				</article>
			</div>
		</div>
	</div> <!-- row -->
	
	<button class="btn btn-primary float-right" id="btn-finish-checkout">Finish</button>
	<div class="clear-float"></div>
	
	<!-- ========================= FOOTER ========================= -->
	<?php include('footer.php');?>
</body>
</html>