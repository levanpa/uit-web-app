
<footer class="section-footer bg2" id="section-footer">
	<div class="container">
		<section class="footer-bottom row">
			<div class="col-sm-3" > 
				<h5>ABOUT HORLUX</h5>
				<a href="#">About us</a><br>
				<a href="#">Secure Policy</a><br>
				<a href="#">Agreement</a><br>
				<a href="#">Recruitment</a><br>
			</div>
			<div class="col-sm-3">
				<h5>HELP AND CONTACT</h5>
				<a href="#">FAQ</a><br>
				<a href="#">Contact and feedback</a><br>
				<a href="#">Payment and delivery</a><br>
			</div>
			<div class="col-sm-3" id="social-content">
				<h5>SOCIAL NETWORK</h5>
				<i class="fab fa-facebook"></i>
				<a href="#">Facebook</a><br>
				<i class="fab fa-twitter"></i>
				<a href="#">Twitter</a><br>
				<i class="fab fa-google-plus"></i>
				<a href="#">Google+</a><br>
				<i class="fab fa-linkedin"></i>
				<a href="#">LinkedIn</a><br>
				<i class="fab fa-instagram"></i>
				<a href="#">Instagram</a><br>
			</div>
			<div class="col-sm-3">
				<img class="logo-gov" src="images/logos/online-gov.png">
				<p>Office: 124 Dragon Rise, Sword Item Reverse Lake, Hanoi</p>
			</div>
		</section> <!-- //footer-top -->
	</div><!-- //container -->
</footer>
