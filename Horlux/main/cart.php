<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Horlux - The best BIDDING place ever</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

	<!-- Bootstrap-->
	<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
	<link href="css/bootstrap-custom.css" rel="stylesheet" type="text/css"/>

	<!-- Font awesome -->
	<link href="fonts/fontawesome/css/all.css" type="text/css" rel="stylesheet">
	
	<!-- custom style -->
	<link href="css/uikit.css" rel="stylesheet" type="text/css"/>
	<link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />
	<link rel="stylesheet" type="text/css" href="css/custom.css">

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
	<!-- section-header.// -->
	<?php include('header.php');?>

	<!-- ========================= SECTION CONTENT ========================= -->
	<section class="section-content bg padding-y border-top">
		<div class="container">
			<div class="row">
				<main class="col-md-8 col-xs-12">
					<div class="card">
						<table class="table table-hover table-responsive shopping-cart-wrap">
							<thead class="text-muted">
								<tr>
									<th scope="col">Product</th>
									<th scope="col" width="120">Price</th>
									<th scope="col" class="text-right" width="200">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<figure class="media">
											<div class="img-wrap"><img src="images/items/1.jpg" class="img-thumbnail img-sm"></div>
											<figcaption class="media-body">
												<h6 class="title text-truncate">Product name goes here </h6>
												<dl class="dlist-inline small">
													<dt>Size: </dt>
													<dd>XXL</dd>
												</dl>
												<dl class="dlist-inline small">
													<dt>Color: </dt>
													<dd>Orange color</dd>
												</dl>
											</figcaption>
										</figure> 
									</td>
									<td> 
										<div class="price-wrap"> 
											<var class="price">145000 vnd</var> 
										</div> <!-- price-wrap .// -->
									</td>
									<td class="text-right"> 
										<a href="" class="btn btn-outline-danger"> × Remove</a>
									</td>
								</tr>
								<tr>
									<td>
										<figure class="media">
											<div class="img-wrap"><img src="images/items/2.jpg" class="img-thumbnail img-sm"></div>
											<figcaption class="media-body">
												<h6 class="title text-truncate">Product name goes here </h6>
												<dl class="dlist-inline small">
													<dt>Size: </dt>
													<dd>XXL</dd>
												</dl>
												<dl class="dlist-inline small">
													<dt>Color: </dt>
													<dd>Orange color</dd>
												</dl>
											</figcaption>
										</figure> 
									</td>
									<td> 
										<div class="price-wrap"> 
											<var class="price">35000 vnd</var> 
										</div> <!-- price-wrap .// -->
									</td>
									<td class="text-right"> 
										<a href="" class="btn btn-outline-danger btn-round"> × Remove</a>
									</td>
								</tr>
								<tr>
									<td>
										<figure class="media">
											<div class="img-wrap"><img src="images/items/3.jpg" class="img-thumbnail img-sm"></div>
											<figcaption class="media-body">
												<h6 class="title text-truncate">Product name goes here </h6>
												<dl class="dlist-inline small">
													<dt>Size: </dt>
													<dd>XXL</dd>
												</dl>
												<dl class="dlist-inline small">
													<dt>Color: </dt>
													<dd>Orange color</dd>
												</dl>
											</figcaption>
										</figure> 
									</td>

									<td> 
										<div class="price-wrap"> 
											<var class="price">45000 vnd</var> 
										</div> <!-- price-wrap .// -->
									</td>
									<td class="text-right"> 
										<a href="" class="btn btn-outline-danger btn-round"> × Remove</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div> <!-- card.// -->
				</main> <!-- col.// -->
				<aside class="col-md-4 col-xs-12">
					<p class="alert alert-success">Free shipping fee for order with the price is $100 or above!</p>
					<dl class="dlist-align">
						<dt>Total price: </dt>
						<dd class="text-right">568000 vnd</dd>
					</dl>
					<dl class="dlist-align">
						<dt>Shipping fee: </dt>
						<dd class="text-right">0 vnd</dd>
					</dl>
					<dl>
						<dt id="coupon-code">Coupon code: </dt>
						<form class="form float-right">
							<input type="text" class="form-control" id="btn-coupon-code" value="discount3004">
						</form>
					</dl>
					<dl class="dlist-align">
						<dt>Discount:</dt>
						<dd class="text-right">50000 vnd</dd>
					</dl>
					<dl class="dlist-align h4">
						<dt>Total:</dt>
						<dd class="text-right"><strong> 518000 vnd</strong></dd>
					</dl>
					<hr>
					<dl class="dlist-align h4">
						<a href="checkout.php" class="btn btn-primary float-right" role="button">Checkout all</a>
						<div class="clear-float"></div>
					</dl>
					<p class="alert alert-success">We will delivery to your primary shipping address by default.</p>

					<div class="card mb-3">
						<header class="card-header">
							<a href="#" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" class="collapsed">
								<i class="icon-action fa fa-chevron-down"></i>
								<h6 class="title">Change shipping address </h6>
							</a>
						</header>
						<div class="collapse" id="collapse11" style="">
							<article class="card-body">
								<form class="mb15">
									<div class="input-group">
										<input type="text" class="form-control" placeholder="New address">
										<div class="input-group-append">
											<button class="btn btn-primary" type="button">Change</button>
										</div>
									</div>
								</form>	
							</article> <!-- card-body.// -->
						</div> <!-- collapse .// -->
					</div> <!-- card.// -->

					<figure class="itemside mb-3">
						<aside class="aside"><img src="images/icons/pay-visa.png"></aside>
						<div class="text-wrap small text-muted">
							Pay 84.78 AED ( Save 14.97 AED )
							By using ADCB Cards 
						</div>
					</figure>
					<figure class="itemside mb-3">
						<aside class="aside"> <img src="images/icons/pay-mastercard.png"> </aside>
						<div class="text-wrap small text-muted">
							Pay by MasterCard and Save 40%. <br>	
						</div>
					</figure>
				</aside> <!-- col.// -->
			</div>
		</div> <!-- container .//  -->
	</section>
	<!-- ========================= SECTION CONTENT END// ========================= -->

	<!-- ========================= SECTION  ========================= -->
	<section class="section-name bg-white padding-y" id="seccc">
		<div class="container">
		</div><!-- container // -->
	</section>
	<!-- ========================= SECTION  END// ========================= -->

	<!-- ========================= FOOTER ========================= -->
	<?php include('footer.php');?>

</body>
</html>