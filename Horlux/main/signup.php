<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Horlux - The best BIDDING place ever</title>
  <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

  <!-- Bootstrap-->
  <script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
  <link href="css/bootstrap-custom.css" rel="stylesheet" type="text/css"/>

  <!-- Font awesome -->
  <link href="fonts/fontawesome/css/all.css" type="text/css" rel="stylesheet">
  
  <!-- custom style -->
  <link href="css/uikit.css" rel="stylesheet" type="text/css"/>
  <link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />
  <link rel="stylesheet" type="text/css" href="css/custom.css">

  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
  <?php include('header.php');?>
  
  <div class="d-flex justify-content-center">
    <div class="card card-log">
      <article class="card-body mx-auto" style="max-width: 400px;">
        <h4 class="card-title mt-3 text-center">Create Account</h4>
        <p class="text-center">Get started with your free account</p>
        <form name="signup-form" action="" onsubmit="return validateForm();">
          <div class="form-group input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-user"></i></span>
            </div>
            <input name="signup-name" class="form-control" placeholder="Full name" type="text" required>
          </div> <!-- form-group// -->
          <div class="form-group input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-envelope"></i> </span>
            </div>
            <input name="signup-email" class="form-control" placeholder="Email address" type="email">
          </div> <!-- form-group// -->
          <div class="form-group input-group" id="signup-phone">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-phone"></i> </span>
            </div>
            <input name="signup-phone" class="form-control" placeholder="Phone number" type="number" onchange="checkPhone();" required>
          </div> <!-- form-group// -->
          <div class="form-group input-group">            
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-building"></i> </span>
            </div>
            <input type="text" class="form-control" name="signup-address" placeholder="Address">            
          </div> <!-- form-group// -->
          <div class="form-group input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-building"></i> </span>
            </div>
            <select class="form-control" name="signup-job">
              <option selected=""> Select job type</option>
              <option>Designer</option>
              <option>Manager</option>
              <option>Accaunting</option>
            </select>
          </div> <!-- form-group end.// -->
          <div class="form-group input-group" id="signup-pass-area">
            <div class="input-group-prepend">
              <span class="input-group-text"> <i class="fa fa-lock"></i> </span>
            </div>
            <input name="signup-pass" class="form-control" placeholder="Create password" type="password" onchange="checkPass();" required>
            <input name="signup-repass" class="form-control" placeholder="Repeat password" type="password" onchange="checkPass();" required>        
          </div> <!-- form-group// -->                                      
          <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block"> Create Account  </button>
          </div> <!-- form-group// -->
          <p id="error" class="text-error"></p>                                                 
        </form>
      </article>
    </div> <!-- card.// -->
  </div>


  <?php include('footer.php');?>

  <script type="text/javascript" src="js/custom.js"></script>
</body>
</html>