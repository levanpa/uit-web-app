<!DOCTYPE HTML>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Horlux - The best BIDDING place ever</title>
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

	<!-- Bootstrap-->
	<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
	<link href="css/bootstrap-custom.css" rel="stylesheet" type="text/css"/>

	<!-- Font awesome -->
	<link href="fonts/fontawesome/css/all.css" type="text/css" rel="stylesheet">
	
	<!-- custom style -->
	<link href="css/uikit.css" rel="stylesheet" type="text/css"/>
	<link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />
	<link rel="stylesheet" type="text/css" href="css/custom.css">

	<!-- jQuery -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body>
	<!-- section-header.// -->
	<?php include('header.php');?>


	<div class="card" id="bid-area">
		<div class="row no-gutters">
			<aside class="col-sm-5 border-right">
				<article class="gallery-wrap"> 					
					<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
							<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
						</ol>
						<div class="carousel-inner">
							<div class="carousel-item active">
								<img class="d-block w-100" src="images/items/1.jpg" alt="First slide">
							</div>
							<div class="carousel-item">
								<img class="d-block w-100" src="images/items/2.jpg" alt="Second slide">
							</div>
							<div class="carousel-item">
								<img class="d-block w-100" src="images/items/4.jpg" alt="Third slide">
							</div>
						</div>
						<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div> <!-- slider-product.// -->
					<!-- 					<div class="img-small-wrap">
						<div class="item-gallery"> <img src="images/items/1.jpg"></div>
						<div class="item-gallery"> <img src="images/items/2.jpg"></div>
						<div class="item-gallery"> <img src="images/items/3.jpg"></div>
						<div class="item-gallery"> <img src="images/items/4.jpg"></div>
					</div> slider-nav.// -->
				</article> <!-- gallery-wrap .end// -->
			</aside>
			<aside class="col-sm-7">
				<article class="p-5">
					<h3 class="title mb-3">The name of product</h3>

					<div class="row" name="bidding-place">
						<div class="mb-3 col-sm-7"> 
							<var class="price h3 text-primary"> 
								<span class="num top-bid">50000</span><span class="currency"> vnd</span>
							</var>
							<p>Top bid: <span class="num">Vietnam_Jack</span></p>
							<p class="num countdown-time">Ending in 02:32</p>
						</div> <!-- price-detail-wrap .// -->
						<div class="col-sm-5 box bid-box">
							<!-- 	<p>Want to beat him?</p> -->
							<form class="form">
								<input type="text" class="form-control bid-amount" value="55000">
								<button class="btn arrow-left" type="button">
									<a href="#"><i class="fas fa-arrow-left"></i></a>
								</button>
								<button class="btn arrow-right" type="button">
									<a href="#"><i class="fas fa-arrow-right"></i></a>
								</button>
								<input type="button" class="btn btn-primary bid-submit" value="Beat him!">	
							</form>
						</div>
					</div>

					<hr>
					<h3>Bidding history:</h3>
					<table class="table table-striped table-hover" id="bidding-history">
						<tr>
							<th>User</th>
							<th>Price</th>
							<th>Time</th>
						</tr>
						<tr>
							<td>Vietnam_Jack</td>
							<td>50000</td>
							<td>10:30 am today</td>
						</tr>
						<tr>
							<td>Lois</td>
							<td>30000</td>
							<td>10:30 am today</td>
						</tr>
						<tr>
							<td>Hello_world</td>
							<td>20000</td>
							<td>10:28 am today</td>
						</tr>
						<tr>
							<td>Cleveland</td>
							<td>15000</td>
							<td>10:25 am today</td>
						</tr>
					</table>

					<hr>
					<dl>
						<dt>Description</dt>
						<dd><p>Here goes description consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco </p></dd>
					</dl>
					<dl class="row">
						<dt class="col-sm-3">Model#</dt>
						<dd class="col-sm-9">12345611</dd>

						<dt class="col-sm-3">Color</dt>
						<dd class="col-sm-9">Blue and white </dd>

						<dt class="col-sm-3">Delivery</dt>
						<dd class="col-sm-9">Russia, USA, and Europe </dd>
					</dl>
					<!-- <div class="rating-wrap">

						<ul class="rating-stars">
							<li style="width:80%" class="stars-active"> 
								<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
								<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
								<i class="fa fa-star"></i> 
							</li>
							<li>
								<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
								<i class="fa fa-star"></i> <i class="fa fa-star"></i> 
								<i class="fa fa-star"></i> 
							</li>
						</ul>
						<div class="label-rating">132 reviews</div>
						<div class="label-rating">154 orders </div>
					</div> --> <!-- rating-wrap.// -->
					<!-- <div class="row" id="size-and-quantity">
											<div class="col-sm-5">
												<dl class="dlist-inline">
													<dt>Quantity: </dt>
													<dd> 
														<select class="form-control form-control-sm" style="width:70px;">
															<option> 1 </option>
															<option> 2 </option>
															<option> 3 </option>
														</select>
													</dd>
												</dl>  item-property .//
											</div> col.//
											<div class="col-sm-7">
												<dl class="dlist-inline">
													<dt>Size: </dt>
													<dd>
														<label class="form-check form-check-inline">
															<input class="form-check-input" name="inlineRadioOptions" id="inlineRadio2" value="option2" type="radio">
															<span class="form-check-label">SM</span>
														</label>
														<label class="form-check form-check-inline">
															<input class="form-check-input" name="inlineRadioOptions" id="inlineRadio2" value="option2" type="radio">
															<span class="form-check-label">MD</span>
														</label>
														<label class="form-check form-check-inline">
															<input class="form-check-input" name="inlineRadioOptions" id="inlineRadio2" value="option2" type="radio">
															<span class="form-check-label">XXL</span>
														</label>
													</dd>
												</dl>  item-property .//
											</div> col.//
					</div>	 -->					

				</article> <!-- card-body.// -->
			</aside> <!-- col.// -->
		</div> <!-- row.// -->
	</div> <!-- card.// -->
	
	<?php include('footer.php');?>

	<script src="js/custom.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			//adaptive height
			var w = $(document).width();
			if (w <= 576) {
				$(".p-5").removeClass("p-5");
			}

			//check if bidding amount is valid
			$(".bid-submit").click(function(){
				var bidAmount = parseInt($(".bid-amount").val());
				var topBid = parseInt($(".top-bid").text());
				if (bidAmount > topBid) {
					$(".top-bid").text(bidAmount);
					$(".bid-amount").val(bidAmount + 10000);
					$(".bid-amount").css("background-color","#fff");
				} else {
					$(".bid-amount").css("background-color","#ffcccc");
				}
			});

			//adjust bidding amount
			$(".arrow-left").click(function(){
				var bidAmount = parseInt($(".bid-amount").val());
				if (bidAmount > 10000) {
					$(".bid-amount").val(bidAmount-10000);
					$(".bid-amount").css("background-color","#fff");
				} else {
					$(".bid-amount").css("background-color","#ffcccc");
				}
			});
			$(".arrow-right").click(function(){
				var bidAmount = parseInt($(".bid-amount").val());
				if (bidAmount < 100000000) {
					$(".bid-amount").val(bidAmount+10000);
					$(".bid-amount").css("background-color","#fff");
				} else {
					$(".bid-amount").css("background-color","#ffcccc");
				}
			});

		});
	</script>
</body>
</html>